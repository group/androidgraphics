package ie.tcd.scss.mcmahob1.drawing2d;

import java.lang.Math;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Parcel;
import android.view.MotionEvent;
import android.view.View;

//test merge
public class BouncingBallView extends View {
    public int orange = Color.parseColor("#FF9800");
    public int cyan = Color.parseColor("#0097A7");

    public Ball[] ball = new Ball[24];
    public Box[] box = new Box[65];
    public float previousX, previousY;
    public int color = cyan;
    public int king = Color.BLACK;
    public Paint text;
    public int width =0;
    public int height=0;
    public int num;
    public int stat;
    public int nav;
    public int win=0;

    public int a; //= (768 / 10); //768 is the screen size of nexus 4

    // Constructor
    public BouncingBallView(Context context) {
        super(context);
//----------------------Code for getting resize variables------------------------------------------------------//
        Rect rect=new Rect();
        getWindowVisibleDisplayFrame(rect);
        width=rect.width();
        height=rect.height();
        a=Math.min(width,height)/10;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            nav= resources.getDimensionPixelSize(resourceId);
        }
        resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            stat= resources.getDimensionPixelSize(resourceId);
        }
//-------------------------------------------------------------------------------------------//
        setupBoxes();      //boxes must be setup first because circles initialise to locations of boxes (bad design?)
        setupCircles();
//---------------------------------------Setup Text Display------------------------------------//
        text = new Paint();
        text.setTextAlign(Paint.Align.CENTER);
        text.setTypeface(Typeface.MONOSPACE);
        text.setTextSize(width/13);
        text.setColor(Color.BLACK);

        // To enable touch mode
        this.setFocusableInTouchMode(true);
    }

    public BouncingBallView(Context context, Parcel in) {
        super(context);
//---------------------------------Get variables for resizing--------------------------------//
        Rect rect=new Rect();
        getWindowVisibleDisplayFrame(rect);
        width=rect.width();
        height=rect.height();
        a=Math.min(width,height)/10;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            nav= resources.getDimensionPixelSize(resourceId);
        }
        resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            stat= resources.getDimensionPixelSize(resourceId);
        }
//--------------------------------------------------------------------------------------------//
        setupBoxes();      //boxes must be setup first because circles initialise to locations of boxes (bad design?)
        //setupCircles();  //Don't setup circles as circle data is loaded from parcel
//--------------------------------Load Parcel Data-------------------------------------------//
        color=in.readInt();
        king=in.readInt();
	    ball=in.createTypedArray(Ball.CREATOR);
//---------------------------------Setup Text------------------------------------------------//
        text = new Paint();
        text.setTextAlign(Paint.Align.CENTER);
        text.setTypeface(Typeface.MONOSPACE);
        text.setTextSize(width/13);
        text.setColor(Color.BLACK);

        box[0].paint.setColor(color);   //load color value taken from parel

        // To enable touch mode
        this.setFocusableInTouchMode(true);
    }

    public void writeToParcel(Parcel out, int Flag) {       //for saving data to be able to resume
        out.writeInt(color);
        out.writeInt(king);
        out.writeTypedArray(ball, 0);
    }


    // Called back to draw the view. Also called after invalidate().
    @Override
    protected void onDraw(Canvas canvas) {
        //loop through the boxes and draw them where they are
        for (int n = 0; n < 65; n++) {
            box[n].draw(canvas);
        }
        //loop through the balls (checkers) and draw them where they are
        for (int n = 0; n < 24; n++) {
            ball[n].draw(canvas);
        }
//---------------------------------------------------Text drawing-------------------------------------------------------------------
        if(win==2) {
            text.setColor(cyan);
            canvas.drawText("Cyan player wins", canvas.getWidth() / 2, (Math.max(width, height) + stat - nav) * 3 / 4 + a * 2, text);
            canvas.rotate(180f);
            canvas.drawText("Cyan player wins", -canvas.getWidth() / 2, (-a * 2), text);
            canvas.rotate(180f);
        }
        else if(win==1) {
            text.setColor(orange);
            canvas.drawText("Orange player wins", canvas.getWidth() / 2, (Math.max(width, height) + stat - nav) * 3 / 4 + a * 2, text);
            canvas.rotate(180f);
            canvas.drawText("Orange player wins", -canvas.getWidth() / 2, (-a * 2), text);
            canvas.rotate(180f);
        }

        else if(color==cyan)
            canvas.drawText("Cyan player's turn", canvas.getWidth()/2,(Math.max(width,height)+stat-nav)*3/4+a*2, text);

        else if(color==orange) {
            canvas.rotate(180f);
            canvas.drawText("Orange player's turn", -canvas.getWidth() / 2, (- a * 2), text);
            canvas.rotate(180f);
        }
//------------------------------------------------------------------------------------------------------------------------------

        invalidate();  // Force a re-draw, has to be called to render next frame
    }

    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        // Set the size of the background to fill screen
        box[0].set(0, 0, w, h);
    }

    // Touch-input handler
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {                                //get the action
            case MotionEvent.ACTION_DOWN:                           //if it's a press
                num = findNearestBall(event.getX(), event.getY());  //get the index of the ball
                previousX = ball[num].x;                            //get where the ball was
                previousY = ball[num].y;
                break;
            case MotionEvent.ACTION_MOVE:                           //if it's a move while held down
                ball[num].x = event.getX();                         //move the ball with our finger
                ball[num].y = event.getY();
                break;
            case MotionEvent.ACTION_UP:                             //when piece is released

                if (rules(event.getX(), event.getY(), previousX, previousY)) {  //if the move abides by all rules

                    int i = findNearestSquare(event.getX(), event.getY());      //find the index of the nearest box

                    ball[num].x = box[i].xMin + (a / 2);
                    ball[num].y = box[i].yMin + (a / 2);    //move selected ball to selected box
//---------------------------------------Change of
                    int b = 0;
                    int r = 0;
                    for (int n = 0; n < 12; n++) {          //check if player 1 has any pieces left
                        if (ball[n].radius > b)             //radius is set to 0 if ball doesn't exist anymore
                            b = 1;
                    }
                    for (int n = 12; n < 24; n++) {         //check if player 2 has any pieces left
                        if (ball[n].radius > r)
                            r = 1;
                    }
                    if (r == 0) {                           //if player 2 has no pieces
                        box[0].paint.setColor(Color.WHITE);
                        win=1;                              //The winner is player 1

                    } else if (b == 0){                     //if player 1 has no pieces
                        box[0].paint.setColor(Color.BLACK);
                        win=2;                              //player 2 wins

                    }else if (color == cyan) {              //If the previous player was cyan
                        color = orange;                     //change turn to orange
                        king = Color.RED;                   //oranges kings are red, to help with rules() function
                        box[0].paint.setColor(orange);      //Change background to oragne
                    } else {                                //Same thing, but for other player
                        color = cyan;
                        king = Color.BLACK;//700cyan
                        box[0].paint.setColor(cyan);
                    }
                } else {                                //If the move doesn't satisfy the rules return ball to previus position
                    ball[num].x = previousX;
                    ball[num].y = previousY;
                } //do nothing if rules aren't met
                break;
        }
        return true;  // Event handled
    }

    void setupBoxes() {
        box[0] = new Box(cyan);                         //Setup background, cyan for player 1
        int BOX = 1;
//------------------------------Correctly colour boxes------------------------------------------
        for (int n = 0; n < 8; n++) {
            if (n % 2 == 0) {
                for (int i = 0; i < 8; i++) {
                    if (i % 2 == 0)
                        box[BOX] = new Box(Color.WHITE);
                    else
                        box[BOX] = new Box(Color.BLACK);
                    BOX++;
                }
            } else {
                for (int i = 0; i < 8; i++) {
                    if (i % 2 != 0)
                        box[BOX] = new Box(Color.WHITE);
                    else
                        box[BOX] = new Box(Color.BLACK);
                    BOX++;
                }
            }

        }
//-----------------------------Correctly position boxes----------------------------------------
        int x = 1;
        int buffer=(Math.max(width,height)+stat-nav)/2-a*5;
        for (int i = 1; i < 9; i++) {
            for (int n = 1; n < 9; n++) {
                box[x].set(n * a, a * i+buffer, n * a + a, a * i + a+buffer);
                x++;
            }
        }
    }

    void setupCircles() {
//--------------------------Colur the circles----------------------------------------
        for (int i = 0; i < 12; i++)
            ball[i] = new Ball(orange);//500red
        for (int i = 12; i < 24; i++)
            ball[i] = new Ball(cyan);
//--------------------------Position the circles on the squares---------------------
        int BALL = 0;
        for (int i = 1; i < 25; i++) {
            if (box[i].paint.getColor() == Color.WHITE) {
                ball[BALL].x = box[i].xMin + (a / 2);
                ball[BALL].y = box[i].yMin + (a / 2);
                ball[BALL].radius=3*a/7;
                BALL++;
            }
        }

        for (int i = 42; i < 65; i++) {
            if (box[i].paint.getColor() == Color.WHITE) {
                ball[BALL].x = box[i].xMin + (a / 2);
                ball[BALL].y = box[i].yMin + (a / 2);
                ball[BALL].radius=3*a/7;
                BALL++;
            }
        }
    }

    int findNearestBall(float x, float y) {                         //Find the piece closest to position of touch input, returns the index of that piece
        PointF closest = new PointF(ball[0].x - x, ball[0].y - y);
        int index = 0;
        for (int i = 1; i < 24; i++) {
            PointF b = new PointF(ball[i].x - x, ball[i].y - y);
            if (closest.length() > b.length()) {
                closest = b;
                index = i;
            }
        }
        return index;
    }

    int findNearestSquare(float x, float y) {                       //Find the square closest to touch input, returns index of square

        int index = 0;
        for (int i = 1; i < 65; i++) {
            if (box[i].xMin < x && x < box[i].xMax && box[i].yMin < y && y < box[i].yMax)
                index = i;
        }
        return index;
    }

    boolean rules(float x, float y, float pX, float pY) {

        if (ball[num].paint.getColor() == color || ball[num].paint.getColor() == king) { //if it's that pieces turn

            int b = findNearestSquare(x, y); //all the following lines basically check that the next square is beside the current

            if (ball[num].paint.getColor() == orange) {
                int start=12;
                int finish=24;
                return downMove(num, b, pX, pY, start, finish);
            }

            if (ball[num].paint.getColor() == cyan) {
                int start=0;
                int finish=12;
                return upMove(num, b, pX, pY, start, finish );
            }

            if (ball[num].paint.getColor() == Color.RED)//700orange
            {
                int start=12;
                int finish=24;
                return upMove(num, b, pX, pY, start, finish) || downMove(num, b, pX, pY, start, finish);
            }

            if (ball[num].paint.getColor() == Color.BLACK) {
                int start=0;
                int finish=12;
                return upMove(num, b, pX, pY, start, finish) || downMove(num, b, pX, pY, start, finish);
    }

        }
        return false;
    }

    boolean upMove(int num, int b, float pX, float pY, int start, int finish) {         //Checks validity of moves in up direction. < 3 is used to check approximate equality of things, as there is rounding error due to integer division
        if (Math.abs(box[b].yMax + (a / 2) - pY) < 3 && (Math.abs(box[b].xMax + (a / 2) - pX) < 3  || Math.abs(box[b].xMin - (a / 2) - pX) < 3)) { //If box is above AND (to left or right) of initial box piece was on
            for (int i = 0; i < 24; i++) {                                              //Check if another piece is in that position
                if (Math.abs(ball[i].y - box[b].yMin - (a / 2))<3 && Math.abs(ball[i].x - box[b].xMin - (a / 2))<3) {
                    return false;
                }
            }


            if (b < 9 && ball[num].paint.getColor() == cyan)
                ball[num].paint.setColor(Color.BLACK);  //change color to indicate a BLACK king

            return true;
        }
        //-----------------------------Jumping a piece-----------------------------------
        else if (Math.abs(box[b].yMax + (a * 1.5) - pY)<3) {                          //If the square you're moving to is two spaces forward and...

            if (Math.abs(box[b].xMax + (a * 1.5) - pX) <3) {                          //... Two to the right

                for (int i = start; i < finish; i++) {

                    if (Math.abs(ball[i].y + a - pY)<3 && Math.abs(ball[i].x + a - pX)<3) { //Check if there's a piece inbetween to jump

                        //destroy piece in betwween
                        ball[i].x = 0;
                        ball[i].y = 0;
                        ball[i].radius = 0;

                        if (b < 9 && ball[num].paint.getColor() == cyan)                    //Check if the piece can be kinged
                            ball[num].paint.setColor(Color.BLACK);
                        return true;
                    }

                }
            } else if (Math.abs(box[b].xMin - (a * 1.5) - pX )<3) {                         //Same, but going left

                for (int i = start; i < finish; i++) {

                    if (Math.abs(ball[i].y + a - pY) <3 && Math.abs(ball[i].x - a - pX)<3) {

                        ball[i].x = 0;
                        ball[i].y = 0;
                        ball[i].radius = 0;

                        if (b < 9 && ball[num].paint.getColor() == cyan)
                            ball[num].paint.setColor(Color.BLACK);

                        return true;
                    }

                }
            }

        }
        return false;

    }

    boolean downMove(int num, int b, float pX, float pY, int start, int finish){        //Same as upMove() but with y-axis inverted and colours set to player 2

                if (Math.abs(box[b].yMin - (a / 2) - pY) <3 && (Math.abs(box[b].xMax + (a / 2) - pX) <3 || Math.abs(box[b].xMin - (a / 2) - pX) < 3)) {
                    //box[b].paint.setColor(cyan);

                    for (int i = 0; i < 24; i++) {

                        if (Math.abs(ball[i].y - box[b].yMin - (a / 2)) <3 && Math.abs(ball[i].x - box[b].xMin - (a / 2))<3) {

                            //box[b].paint.setColor(Color.BLU

                            return false;

                        }

                    }
                    //orange;//700orange

                    if (b > 57 && ball[num].paint.getColor() == orange)
                        ball[num].paint.setColor(Color.RED);  //change color to indicate a red king

                    return true;
                } else if (Math.abs(box[b].yMin - (a * 1.5) - pY) <3) {

                    if (Math.abs(box[b].xMax + (a * 1.5) - pX)<3) {

                        for (int i = start; i < finish; i++) {

                            if (Math.abs(ball[i].y - a - pY) <3 && Math.abs(ball[i].x + a - pX)<3) {

                                ball[i].x = 0;
                                ball[i].y = 0;
                                ball[i].radius = 0;

                                if (b > 57 && ball[num].paint.getColor() == orange)
                                    ball[num].paint.setColor(Color.RED);

                                return true;
                            }

                        }
                    } else if (Math.abs(box[b].xMin - (a * 1.5) - pX) <3) {

                        for (int i = start; i < finish; i++) {

                            if (Math.abs(ball[i].y - a - pY) <3 && Math.abs(ball[i].x - a - pX)<3) {

                                ball[i].x = 0;
                                ball[i].y = 0;
                                ball[i].radius = 0;


                                if (b > 57 && ball[num].paint.getColor() == orange)
                                    ball[num].paint.setColor(Color.RED);
                                //red500 and orange700
                                return true;
                            }

                        }
                    }

                }
        return false;
    }
}


