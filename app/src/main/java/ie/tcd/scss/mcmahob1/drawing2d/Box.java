package ie.tcd.scss.mcmahob1.drawing2d;


import android.graphics.Canvas;
        import android.graphics.Paint;
        import android.graphics.Rect;

public class Box {
    int xMin, xMax, yMin, yMax;           //Defines positions of corners of our square (xMin,yMin), (xMin,yMax),(xMax,yMin),(xMax,yMax)
    Paint paint;  // paint style and color
    private Rect bounds;                 //needed for canvas

    public Box(int color) {
        paint = new Paint();
        paint.setColor(color);
        bounds = new Rect();
    }

    public void set(int x, int y, int w, int h) {
        xMin=x;
        yMin=y;
        xMax=w;
        yMax=h;
        bounds.set(x, y, w, h);
    }

    public void draw(Canvas canvas) {
        bounds.set(xMin, yMin, xMax, yMax);
        canvas.drawRect(bounds, paint);
    }
}
