package ie.tcd.scss.mcmahob1.drawing2d;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;

public class Ball implements Parcelable {
    float radius = 30;      // Ball's radius
    float x = 50;  // Ball's center (x,y)
    float y = 50;
    private RectF bounds;   // Needed for Canvas.drawOval
    public Paint paint;    // The paint style, color used for drawing

    // Constructor
    public Ball(int color) {
        bounds = new RectF();
        paint = new Paint();
        paint.setColor(color);
    }

    public int describeContents() {
        return 0;
    }       //Has to be here for parcelable

    public void writeToParcel(Parcel out, int flags) {//for saving ball to parcel
        out.writeFloat(radius);
        out.writeFloat(x);
        out.writeFloat(y);
        out.writeInt(paint.getColor());
    }

    public static final Parcelable.Creator<Ball> CREATOR = new Parcelable.Creator<Ball>() { //Defines how to unparcel the parcel. Allows for creation of array of Ball objects or individual ball
        public Ball createFromParcel(Parcel in) {
            return new Ball(in);
        }

        public Ball[] newArray(int size) {
            return new Ball[size];
        }
    };

    protected Ball(Parcel in) {         //unparcel a Ball parcel
        radius = in.readFloat();
        x=in.readFloat();
        y=in.readFloat();
        paint = new Paint();
        bounds = new RectF();
        paint.setColor(in.readInt());

    }

    public void draw(Canvas canvas) {
        bounds.set(x-radius, y-radius, x+radius, y+radius);
        canvas.drawOval(bounds, paint);
    }
}
