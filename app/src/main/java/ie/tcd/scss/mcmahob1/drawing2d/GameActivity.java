package ie.tcd.scss.mcmahob1.drawing2d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class GameActivity extends Activity {
public String parcel="parcel";			//Think the string is used to generate a key to be stored in the parcel, so string should have a value?
public Parcel resume;
String FILENAME = "SAVE";

public BouncingBallView bouncingBallView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//-----------------------------Load the saved data to resume parcel (silently fails if no save exists)----------------------------
        try{
            FileInputStream fis=openFileInput(FILENAME);            //open our previously saved game
            byte[] buffer = new byte[(int) fis.getChannel().size()];//
            fis.read(buffer);
            if(buffer.length>1) {
                resume=resume.obtain();
                resume.unmarshall(buffer, 0, buffer.length);
                resume.setDataPosition(0);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//-----------------------------------------------------------------------------

        if(getIntent().getExtras().getBoolean(parcel)&&resume!=null){		//if the resume button is pressed and there is data to resume from, resume, else start new game?
            super.onCreate(savedInstanceState);
            bouncingBallView = new BouncingBallView(this, resume);
            setContentView(bouncingBallView);

        }
        else{                                               //Newgame start
            super.onCreate(savedInstanceState);
            bouncingBallView = new BouncingBallView(this);
            setContentView(bouncingBallView);

        }
    }

    @Override
    protected void onPause(){               //saving of data happens onPause
        super.onPause();
        resume=resume.obtain();             //Make an empty parcel
        bouncingBallView.writeToParcel(resume, 0);//write our game data to the parcel
        try {
            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);//open/create new file in our app install directory
            fos.write(resume.marshall());                                         //write the bits of the parcel to the file
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
