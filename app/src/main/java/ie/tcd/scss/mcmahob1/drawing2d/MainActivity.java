package ie.tcd.scss.mcmahob1.drawing2d;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    public String parcel="parcel";			//Think the value of the string is used to generate a key to be stored in the parcel, so string should have a value? Examples online usually use string literals, eg. putExtra("parcel",true")

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void New(View v) {
        Intent intent = new Intent(this,GameActivity.class);
        intent.putExtra(parcel, false);
        startActivity(intent);

    }

    public void Resume(View v) {

        Intent intent = new Intent(this,GameActivity.class);
        intent.putExtra(parcel, true);
        startActivity(intent);
    }
}
